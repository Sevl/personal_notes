﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalNotes.Model.Filter
{
    public class FinanceFilterModel
    {
        public string ValueFilter { get; set; }
        public string ShowOnly { get; set; }
    }
}
