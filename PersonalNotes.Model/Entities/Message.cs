﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalNotes.Model.Entities
{
    public class Message
    {
        public string Receiver { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
