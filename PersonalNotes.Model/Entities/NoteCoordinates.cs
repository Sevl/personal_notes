﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalNotes.Model.Entities
{
    public class NoteCoordinates
    {
        public int NoteCoordinatesId { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public string Position { get; set; }

        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        public int NoteId { get; set; }
        public virtual Note Note { get; set; }
    }
}
