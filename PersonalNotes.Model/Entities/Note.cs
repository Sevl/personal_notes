﻿namespace PersonalNotes.Model.Entities
{
    public class Note
    {
        public int NoteId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public string Position { get; set; }

        public int AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}