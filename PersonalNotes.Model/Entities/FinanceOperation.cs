﻿using System;

namespace PersonalNotes.Model.Entities
{
    public class FinanceOperation
    {
        public int FinanceOperationId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Value { get; set; }
        public DateTime DateOfOperation { get; set; }


        public int AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}
