﻿using AutoMapper;
using PersonalNotes.Model.Entities;
using PersonalNotes.Model.Identity;

namespace PersonalNotes.Database.Identity.Mapping
{
    public class MappingToUser
    {

        public static IMapper mapper;

        public static IMapper getMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Account, ApplicationUser>().ForMember(au => au.UserName, map => map.MapFrom(vm => vm.UserName));
            });

            mapper = config.CreateMapper();

            return mapper;
        }
    }
}
