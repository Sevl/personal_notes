﻿using AutoMapper;
using PersonalNotes.Model.Entities;
using PersonalNotes.Model.Identity;

namespace PersonalNotes.Database.Identity.Mapping
{
    public class MappingToRole
    {

        public static IMapper mapper;

        public static IMapper getMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Account, ApplicationRole>().ForMember(au => au.Name, map => map.MapFrom(vm => vm.Role));
            });

            mapper = config.CreateMapper();

            return mapper;
        }
    }
}
