﻿using AutoMapper;
using PersonalNotes.Model.Entities;
using PersonalNotes.Model.Identity;

namespace PersonalNotes.Database.Identity.Helpers
{
    public class AccountToIdentityMappingProfile : Profile
    {
        public AccountToIdentityMappingProfile()
        {
            CreateMap<Account, ApplicationUser>().ForMember(au => au.UserName, map => map.MapFrom(vm => vm.UserName))
                .ForMember(au => au.Id, map => map.MapFrom(vm => vm.AccountId));
        }
    }
}
