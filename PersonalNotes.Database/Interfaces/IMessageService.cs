﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PersonalNotes.Model.Entities;

namespace PersonalNotes.Database.Interfaces
{
    public interface IMessageService
    {
        Task<bool> SendMail(Message message);
    }
}
