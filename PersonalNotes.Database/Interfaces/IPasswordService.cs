﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalNotes.Database.Interfaces
{
    public interface IPasswordService
    {
        bool CheckPassword(string id, string password);
        Task<bool> ChangePassword(string id, string password);
    }
}
