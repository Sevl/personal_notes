﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PersonalNotes.Model.Entities;
using PersonalNotes.Model.Filter;

namespace PersonalNotes.Database.Interfaces
{
    public interface IFinanceService
    {
        IEnumerable<FinanceOperation> Operations { get; }
        IEnumerable<FinanceOperation> GetUserOperations(int id); 
        IEnumerable<FinanceOperation> FilterOperationsByDate(int month, int year); 
        IEnumerable<FinanceOperation> FilterOperationsByType(string type); 
        IEnumerable<FinanceOperation> FilterFinanceOperations(FinanceFilterModel ffm); 
        Task<FinanceOperation> FinanceAdd(FinanceOperation finance);
        FinanceOperation FinanceEdit(FinanceOperation finance);
        Task<FinanceOperation> FinanceDelete(int id);
        FinanceOperation FinanceById(int id);
        FinanceOperation FindByObj(FinanceOperation finance);
    }
}
