﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PersonalNotes.Model.Entities;

namespace PersonalNotes.Database.Interfaces
{
    public interface IAccountService
    {
        IEnumerable<Account> Accounts { get; }

        Task<Account> GetAccountById(int id);
        Task<Account> GetAccountByName(string name);

        Account GetAccount(string name, string password);
        Task<Account> AccountSave(Account account);
        Account AccountEdit(int id, Account account);
        Task<Account> AccountDelete(int id);
        bool CheckLogin(string login);
    }
}
