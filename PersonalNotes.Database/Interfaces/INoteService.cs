﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PersonalNotes.Model.Entities;

namespace PersonalNotes.Database.Interfaces
{
    public interface INoteService
    {
        IEnumerable<Note> Notes { get; }

        IEnumerable<Note> GetNotesByAccountId(int id);
        Task<Note> GetNoteById(int id);
        Task<Note> GetNoteByName(string name);
        
        Task<Note> NoteAdd(Note note);
        Note NoteEdit(Note note);
        Task<Note> NoteDelete(int id);
    }
}
