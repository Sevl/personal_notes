﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using PersonalNotes.Database.Interfaces;
using PersonalNotes.Model.Identity;

namespace PersonalNotes.Database.Services
{
    public class PasswordService : IPasswordService
    {
        private readonly PersonalNotesContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public PasswordService(PersonalNotesContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        } 

        public bool CheckPassword(string id, string newPassword)
        {
            
            var user = _userManager.FindByIdAsync(id).Result;
            
            return _userManager.CheckPasswordAsync(user, newPassword).Result;
        }

        public async Task<bool> ChangePassword(string id, string newPassword)
        {
            try
            {
                var user = _userManager.FindByIdAsync(id).Result;

                var acc = _db.Accounts.FirstOrDefault(a => a.UserName == user.UserName);

                if (acc != null && user != null)
                {
                    await _userManager.ChangePasswordAsync(user, acc.Password, newPassword);
                    acc.Password = newPassword;
                }

                _db.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            

        }
    }
}
