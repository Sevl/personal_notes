﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PersonalNotes.Database.Interfaces;
using PersonalNotes.Model.Entities;

namespace PersonalNotes.Database.Services
{
    public class NoteService : INoteService
    {

        private readonly PersonalNotesContext _db;

        public NoteService(PersonalNotesContext db)
        {
            _db = db;
        }

        public IEnumerable<Note> Notes => _db.Notes.ToList();

        public IEnumerable<Note> GetNotesByAccountId(int id) => _db.Notes.OrderBy(n => n.AccountId == id);
        public async Task<Note> GetNoteById(int id) => await _db.Notes.FirstOrDefaultAsync(n => n.NoteId == id);
        public async Task<Note> GetNoteByName(string name) => await _db.Notes.FirstOrDefaultAsync(n => n.Name == name);
       
        public async Task<Note> NoteAdd(Note note)
        {
            await _db.Notes.AddAsync(note);
            await _db.SaveChangesAsync();

            return GetNoteByName(note.Name).Result;
        }

        public Note NoteEdit(Note note)
        {
            var myNote = _db.Notes.SingleOrDefault( n => n.NoteId == note.NoteId);
            if (myNote != null)
            {
                myNote.Left = note.Left;
                myNote.Top = note.Top;
                myNote.Position = note.Position;
                myNote.Name = note.Name;
                myNote.Description = note.Description;
                myNote.Status = note.Status;
            }

            _db.SaveChanges();

            return note;
        }

        public async Task<Note> NoteDelete(int id)
        {
            var note = await _db.Notes.AsNoTracking().FirstOrDefaultAsync(m => m.NoteId == id);

            _db.Notes.Remove(note);
            await _db.SaveChangesAsync();

            return note;
        }
    }
}
