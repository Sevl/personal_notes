﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PersonalNotes.Database.Identity;
using PersonalNotes.Database.Identity.Helpers;
using PersonalNotes.Database.Interfaces;
using PersonalNotes.Model.Entities;
using PersonalNotes.Model.Identity;



namespace PersonalNotes.Database.Services
{
    public class MessageService : IMessageService
    {
        private readonly IAccountService _accountService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly SmtpClient _smtp;
        private readonly MailAddress fromAddress = new MailAddress("test.user.192@mail.ru", "From Server");
        private readonly MailAddress toAddress = new MailAddress("test.user.2@mail.ru", "To Client");

        public MessageService(IAccountService accountService,
                            IJwtFactory jwtFactory,
                            UserManager<ApplicationUser> userManager,
                            IOptions<JwtIssuerOptions> jwtOptions,
                            SignInManager<ApplicationUser> signInManager)
        {
            _jwtFactory = jwtFactory;
            _userManager = userManager;
            _jwtOptions = jwtOptions.Value;
            _accountService = accountService;
            _signInManager = signInManager;

            const string fromPassword = "passw0rd;";
            _smtp = new SmtpClient
            {
                Host = "smtp.mail.ru",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };


        }

        public async Task<bool> SendMail(Message message)
        {
            try
            {
                Console.WriteLine("\n\n" + message.Title + "/" + message.Content + "\n\n");
                Console.WriteLine("\n\nStart sending email\n\n");
                if (message.Type == "ChangePassword")
                {
                    var jwt = GenerateToken(message);
                    if (jwt != null)
                    {
                        //var loginProvider = _signInManager.GetExternalLoginInfoAsync().Result.LoginProvider;
                        var user = _userManager.Users.FirstOrDefault(u => u.UserName == message.Receiver);
                        if (await _userManager.GetAuthenticationTokenAsync(user, "Twitter", user.UserName + "Token") != null)
                        {
                            await _userManager.RemoveAuthenticationTokenAsync(user, "Twitter", user.UserName + "Token");
                        }
                        await _userManager.SetAuthenticationTokenAsync(user, "Twitter", user.UserName + "Token", jwt);
                        //var acc = _accountService.GetAccountByName(message.Receiver).Result;
                        message.Content += "<br>Tab this link if you want to change your password:<br>";
                        Console.WriteLine("JWT TOKEN: " + jwt);
                        message.Content += "<a href='http://localhost:4200/password/"+ jwt + "'>Click here</a>";
                    }
                    else return false;
                    
                    
                }
                using (var mess = new MailMessage(fromAddress, toAddress)
                {
                    Subject = message.Title,
                    Body = message.Content,
                    IsBodyHtml = true
                })
                {
                    _smtp.Send(mess);
                }
                Console.WriteLine("\n\nEmail send successful\n\n");
                
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
                  
        }

        private string GenerateToken(Message message)
        {
            var acc = _accountService.GetAccountByName(message.Receiver).Result;
            if (acc == null)
            {
                return "";
            }

            var identity = GetClaimsIdentity(acc.UserName, acc.Role, acc.Password).Result;
            if (identity == null)
            {
                return "";
            }

            return Tokens.GenerateJwt(identity, _jwtFactory, message.Receiver, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented }).Result;

        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string role, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            Console.WriteLine("\nTry to find user");

            // get the user to verifty
            var userToVerify = await _userManager.FindByNameAsync(userName);

            Console.WriteLine("Result: ");
            Console.Write(userToVerify);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            //var roleToVerify = await _roleManager.FindByNameAsync(role);

            // check the credentials
            Console.WriteLine("\nPASSWORD: " + password +"\n");
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, role, userToVerify.Id));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }
    }
}
