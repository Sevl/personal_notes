﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PersonalNotes.Database.Interfaces;
using PersonalNotes.Model.Entities;

namespace PersonalNotes.Database.Services
{
    public class AccountService : IAccountService
    {
        private readonly PersonalNotesContext _context;

        public AccountService(PersonalNotesContext context) => _context = context;

        public IEnumerable<Account> Accounts => _context.Accounts;

        public async Task<Account> GetAccountByName(string name) =>
            await _context.Accounts.FirstOrDefaultAsync(m => m.UserName == name);

        public async Task<Account> GetAccountById(int id) =>
            await _context.Accounts.FirstOrDefaultAsync(m => m.AccountId == id);

        public async Task<Account> AccountSave(Account account)
        {
            await _context.Accounts.AddAsync(account);
            await _context.SaveChangesAsync();

            return GetAccountByName(account.UserName).Result;
        }

        public Account AccountEdit(int id, Account account)
        {
            Console.WriteLine("\n\nUpdating account");
            Console.WriteLine("[ " + account.AccountId + " ; " + account.UserName + " ; " + account.Role + " ]");
            _context.Entry(account).State = EntityState.Modified;
            _context.SaveChangesAsync();

            return account;
        }

        public async Task<Account> AccountDelete(int id)
        {
            var acc = await _context.Accounts.AsNoTracking().FirstOrDefaultAsync(m => m.AccountId == id);

            _context.Accounts.Remove(acc);
            await _context.SaveChangesAsync();

            return acc;
        }

        public bool CheckLogin(string login)
        {
            var acc = Accounts.FirstOrDefault(ac => ac.UserName == login);
            
            if (acc != null) return false;
            return true;
        }

        public Account GetAccount(string name, string password)
        {
            return _context.Accounts.FirstOrDefault(acc => acc.UserName == name && acc.Password == password);
        }
    }
}
