﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PersonalNotes.Database.Interfaces;
using PersonalNotes.Model.Entities;
using PersonalNotes.Model.Filter;

namespace PersonalNotes.Database.Services
{
    public class FinanceService : IFinanceService
    {
        private PersonalNotesContext _db;

        public FinanceService(PersonalNotesContext db) => _db = db;

        public IEnumerable<FinanceOperation> Operations => _db.FinanceOperations.ToList();

        public IEnumerable<FinanceOperation> GetUserOperations(int id) =>
            _db.FinanceOperations.Where(of => of.AccountId == id).ToList();

        public IEnumerable<FinanceOperation> FilterOperationsByDate(int month, int year)
            => _db.FinanceOperations.Where(of => of.DateOfOperation.Month == month)
                .Where(of => of.DateOfOperation.Year == year).ToList();

        public IEnumerable<FinanceOperation> FilterOperationsByType(string type)
        {
            switch (type)
            {
                case "Income":
                    return _db.FinanceOperations.Where(of => of.Type == "Income").ToList();
                case "Expenditure":
                    return _db.FinanceOperations.Where(of => of.Type == "Expenditure").ToList();
                default:
                    return _db.FinanceOperations.ToList();
            }
        }

        public IEnumerable<FinanceOperation> FilterFinanceOperations(FinanceFilterModel ffm)
        {
            List<FinanceOperation> operations = _db.FinanceOperations.ToList();

            if (ffm.ValueFilter != null)
            {
                switch (ffm.ValueFilter)
                {
                    case "Asc":
                        operations = operations.OrderBy(of => of.Value).ToList();
                        break;
                    case "Desc":
                        operations = operations.OrderByDescending(of => of.Value).ToList();
                        break;
                }
            }

            if (ffm.ShowOnly != null)
            {
                switch (ffm.ShowOnly)
                {
                    case "Income":
                        operations = operations.Where(of => of.Type == "Income").ToList();
                        break;
                    case "Expenditure":
                        operations = operations.Where(of => of.Type == "Expenditure").ToList();
                        break;
                }
            }

            return operations;
        }

        public async Task<FinanceOperation> FinanceAdd(FinanceOperation finance)
        {
            await _db.FinanceOperations.AddAsync(finance);
            await _db.SaveChangesAsync();

            return FindByObj(finance);
        }

        public FinanceOperation FinanceEdit(FinanceOperation finance)
        {
            _db.Entry(finance).State = EntityState.Modified;
            _db.SaveChanges();

            return finance;
        }

        public async Task<FinanceOperation> FinanceDelete(int id)
        {
            var fo = FinanceById(id);
            _db.FinanceOperations.Remove(fo);
            await _db.SaveChangesAsync();

            return fo;
        }

        public FinanceOperation FinanceById(int id)
        {
            return _db.FinanceOperations.FirstOrDefault(fo => fo.FinanceOperationId == id);
        }

        public FinanceOperation FindByObj(FinanceOperation finance)
        {
            return _db.FinanceOperations
                .Where(fo => fo.AccountId == finance.AccountId)
                .Where(fo => fo.Name == finance.Name)
                .Where(fo => fo.Type == finance.Type)
                .Where(fo => fo.DateOfOperation == finance.DateOfOperation).FirstOrDefault(fo => fo.Value == finance.Value);
        }
    }
}
