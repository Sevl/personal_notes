﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PersonalNotes.Model.Entities;
using PersonalNotes.Model.Identity;

namespace PersonalNotes.Database
{
    public class PersonalNotesContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<FinanceOperation> FinanceOperations { get; set; }

        public PersonalNotesContext(DbContextOptions<PersonalNotesContext> options)
            : base(options) { }
    }
}
