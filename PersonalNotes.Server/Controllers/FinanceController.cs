﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PersonalNotes.Database.Interfaces;
using PersonalNotes.Model.Entities;
using PersonalNotes.Model.Filter;

namespace PersonalNotes.Server.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer", Roles = ("Admin, User"))]
    [Route("finance")]
    public class FinanceController : Controller
    {
        private readonly IFinanceService _financeService;

        public FinanceController(IFinanceService financeService)
        {
            _financeService = financeService;
        }

        [HttpGet("{id}")]
        public IEnumerable<FinanceOperation> GetAll(int id) =>  _financeService.GetUserOperations(id);

        [HttpPost]
        public FinanceOperation AddFinanceOperation([FromBody]FinanceOperation financeOperation)
        {
            return _financeService.FinanceAdd(financeOperation).Result;
        }

        [HttpPost("edit")]
        public FinanceOperation EditFinanceOperation([FromBody]FinanceOperation financeOperation)
        {
            return _financeService.FinanceEdit(financeOperation);
        }

        [HttpDelete("{id}")]
        public FinanceOperation DeleteFinanceOperation(int id)
        {
            return _financeService.FinanceDelete(id).Result;
        }

        [HttpPost("filter")]
        public IEnumerable<FinanceOperation> filterOperations([FromBody]FinanceFilterModel ffm) =>
            _financeService.FilterFinanceOperations(ffm);

        [HttpGet("type/{type}")]
        public IEnumerable<FinanceOperation> filterByType(string type) => _financeService.FilterOperationsByType(type);

    }
}
