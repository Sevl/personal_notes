﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PersonalNotes.Database.Interfaces;
using PersonalNotes.Model.Entities;

namespace PersonalNotes.Server.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer", Roles = ("Admin, User"))]
    [Route("note")]
    public class NoteController : Controller
    {
        private readonly INoteService _noteService;

        public NoteController(INoteService noteService)
        {
            _noteService = noteService;
        }

        [HttpGet("{id}")]
        public IEnumerable<Note> GetAll(int id) => _noteService.GetNotesByAccountId(id);

        [HttpPost]
        public Note AddNote([FromBody] Note note) => ModelState.IsValid ? _noteService.NoteAdd(note).Result : null;

        [HttpPost("edit")]
        public Note EditNote([FromBody] Note note) => ModelState.IsValid ? _noteService.NoteEdit(note) : null;

        [HttpGet("get/{id}")]
        public Note GetNoteById(int id) => _noteService.GetNoteById(id).Result;

        [HttpDelete("{id}")]
        public Note DeleteNote(int id) => _noteService.NoteDelete(id).Result;
    }
}
