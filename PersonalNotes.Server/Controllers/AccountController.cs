﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Security.Claims;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PersonalNotes.Database.Identity;
using PersonalNotes.Database.Identity.Helpers;
using PersonalNotes.Database.Identity.Mapping;
using PersonalNotes.Database.Interfaces;
using PersonalNotes.Model.Entities;
using PersonalNotes.Model.Identity;

namespace PersonalNotes.Server.Controllers
{
    [Route("account")]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IMapper _mapper;

        public AccountController(IAccountService accountService,
                                 IJwtFactory jwtFactory,
                                 UserManager<ApplicationUser> userManager,
                                 RoleManager<ApplicationRole> roleManager,
                                 IMapper mapper,
                                 IOptions<JwtIssuerOptions> jwtOptions,
                                 SignInManager<ApplicationUser> signInManager)
        {
            _jwtFactory = jwtFactory;
            _userManager = userManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _jwtOptions = jwtOptions.Value;
            _accountService = accountService;
            _signInManager = signInManager;
        }

        // GET: account
        [HttpGet]
        public IEnumerable<Account> GetAll() => _accountService.Accounts;

        // GET: account/5
        [HttpGet("{id}")]
        public Account GetAccById(int id) => _accountService.GetAccountById(id).Result;

        // GET: account/name
        [HttpGet("get/{name}")]
        public Account GetAccByName(string name)
        {
            Console.WriteLine("\nTry to find user by name\n");
            return _accountService.GetAccountByName(name).Result;
        }

        [HttpGet("check/login/{login}")]
        public bool CheckLogin(string login)
        {
            return _accountService.CheckLogin(login);
        }
        

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]LoginModel model)
        {
            Console.WriteLine("\n\nTry to login\n\n");

            var identity = await GetClaimsIdentity(model.Login, model.Password);
            if (identity == null)
            {
                return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
            }

            var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, model.Login, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });

            /*Console.WriteLine("\nStart working with SignInManager \n");
            var info = _signInManager.GetExternalLoginInfoAsync().Result;
            if (info == null)
            {
                return new BadRequestResult();
            }
            Console.WriteLine("\nLOGIN PROVIDER: " + info.LoginProvider + "\n");*/
            var user = await _userManager.FindByNameAsync(model.Login);
            if (await _userManager.GetAuthenticationTokenAsync(user, "Twitter", user.UserName + "Token") != null)
            {
                await _userManager.RemoveAuthenticationTokenAsync(user, "Twitter", user.UserName + "Token");
            }
            await _userManager.SetAuthenticationTokenAsync(user, "Twitter", user.UserName + "Token", jwt);

            return new OkObjectResult(jwt);
        }

        // POST: account
        [HttpPost]
        public async Task<IActionResult> Register([FromBody]Account account)
        {
            Console.WriteLine("Registration");
            
            var userIdentity = MappingToUser.getMapper().Map<ApplicationUser>(account);

            var userResult = await _userManager.CreateAsync(userIdentity, account.Password);

            if (!userResult.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(userResult, ModelState));

            var roleIdentity = MappingToRole.getMapper().Map<ApplicationRole>(account);
            
            if (!_roleManager.RoleExistsAsync(roleIdentity.Name).Result)
            {
                var roleResult = await _roleManager.CreateAsync(roleIdentity);

                if (!roleResult.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(roleResult, ModelState));
            }
            
            await _userManager.AddToRoleAsync(userIdentity, roleIdentity.Name);

            await _accountService.AccountSave(account);

            return new OkObjectResult("Account created");
        }

        public async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            Console.WriteLine("\nTry to find user");

            // get the user to verifty
            var userToVerify = await _userManager.FindByNameAsync(userName);

            Console.WriteLine("Result: ");
            Console.Write(userToVerify);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            var roleName = _userManager.GetRolesAsync(userToVerify).Result.FirstOrDefault();

            var roleToVerify = await _roleManager.Roles.FirstOrDefaultAsync(r => r.Name == roleName);

            if ( roleToVerify == null ) return await Task.FromResult<ClaimsIdentity>(null);

            // check the credentials
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, roleToVerify.Name, userToVerify.Id));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }

    }
}
