﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PersonalNotes.Database.Interfaces;
using PersonalNotes.Model.Entities;

namespace PersonalNotes.Server.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer", Roles = ("Admin"))]
    [Route("message")]
    public class MessageController : Controller
    {
        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpPost("send")]
        public bool SendMail([FromBody] Message message)
        {
            return _messageService.SendMail(message).Result;
        }
    }
}
