﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PersonalNotes.Database.Interfaces;

namespace PersonalNotes.Server.Controllers
{
    //[Authorize(AuthenticationSchemes = "Bearer", Roles = ("Admin, User"))]
    [Route("password")]
    public class PasswordController : Controller
    {
        private readonly IPasswordService _passwordService;

        public PasswordController(IPasswordService passwordService)
        {
            _passwordService = passwordService;
        }

        [HttpPost("check/{id}")]
        public bool CheckPassword(string id, [FromBody] string password)
        {
            return _passwordService.CheckPassword(id, password);
        }

        [HttpPost("change/{id}")]
        public bool ChangePassword(string id, [FromBody] string password)
        {
            return _passwordService.ChangePassword(id, password).Result;
        }

    }
}
