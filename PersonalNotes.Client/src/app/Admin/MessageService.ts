import { Injectable, ViewChild, ViewChildren } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Message } from '../Admin/Message';


@Injectable()
export class MessageService {
  private baseUrl = 'http://localhost:64086/message';
  headers: Headers = new Headers();
  authToken = localStorage.getItem('auth_token');
  
  
  constructor(private http: Http) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', `Bearer ${this.authToken}`);
  }

  sendMail(message: Message) : Promise<boolean> {
    var headers = this.headers;

    return this.http.post( this.baseUrl + '/send', message, {headers})
              .toPromise()
              .then( res => res.json() as boolean)
              .then( res => {
                  return res;
              }, error => {
                  return error;
              });
  }

}