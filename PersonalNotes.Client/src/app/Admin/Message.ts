export class Message {
    receiver: string;
    type: string;
    title: string;
    content: string;
}