import {Component, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import { Account } from '../Account/Account';
import { MessageService } from '../Admin/MessageService';
import { NgForm } from '@angular/forms';
import { AccountService } from '../Account/AccountService';
import { Message } from '../Admin/Message';

@Component({
  selector: 'admin-acc-list',
  templateUrl: './admin-list.component.html'
})
export class AdminListComponent implements OnInit {
  accounts: Account[];
  message: Message = new Message();
  error: string;
  Token = localStorage.getItem('auth_token');

  /*
    Check
   */
  disableButton = false;
  blockByName = false;
  blockByPassword = false;
  blockAuthorization = false;
  blockBySearchUser = false;
  showSuccess = false;
  showError = false;
  sendMailStatus: boolean;

  public selectdata: any[] = [];

  public types: any[] = [
    { name: 'Notification' },
    { name: 'ChangePassword' },
    { name: 'Reminder' }
  ];

  constructor(private accountService: AccountService,
              private messageService: MessageService
              ) {}

  ngOnInit(): void {
    this.getAccounts();
  }

  getAccounts() : void {
    this.accountService
      .getAccounts()
      .then(response => {
        this.accounts = response;
        this.accounts.forEach(element => {
            this.selectdata.push(element.userName);
        });
        this.showError = false;
      }, error => {
        this.showError = true;
        this.error = error.status + "\n" + error.statusText;
      });
  }

  sendMail(messForm: NgForm) : void {
    this.messageService.sendMail(this.message)
            .then( res => {
              this.sendMailStatus = res;
              messForm.reset();
              setTimeout(function(){
                this.sendMailStatus = undefined;
              },5000);
            }, error => {
              this.sendMailStatus = error;
              setTimeout(function(){
                this.sendMailStatus = undefined;
              },5000);
            });
  }

}
