import {Component, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import { Account } from './Account';
import { NgForm } from '@angular/forms';
import { AccountService } from './AccountService';
import {ActivatedRoute, Router} from '@angular/router';
import { HttpClient } from '@angular/common/http'
import { ModalComponent } from '../Modal/modal.component';

@Component({
  selector: 'app-acc-list',
  templateUrl: './acc-list.component.html'
})
export class AccountListComponent implements OnInit {
  currentAccount: Account[];
  newAccount: Account = new Account();
  LoginAcc: Account = new Account();
  editingAccount: Account = new Account();
  compareAccount: Account = new Account();
  previusPassword: string;
  newPassword: string;
  findLogin: string;
  editing = false;
  error: string;
  Token = localStorage.getItem('auth_token');

  /*
    Check
   */
  disableButton = false;
  blockByName = false;
  blockByPassword = false;
  blockAuthorization = false;
  blockBySearchUser = false;
  showSuccess = false;
  showError = false;

  public selectdata: any[] = [
    { name: 'Admin' },
    { name: 'User' }
  ];

  constructor(private accountService: AccountService,
              ) {}

  ngOnInit(): void {
    this.newAccount.role = this.selectdata[0];
    this.showSuccess = false;
  }

  checkLogin() : void {
    if(this.newAccount.userName != "") {
      this.accountService.checkLogin(this.newAccount.userName)
                  .then(res => {
                      if ( +res == +false ) {
                        this.blockByName = true;
                      } else {
                        this.blockByName = false;
                      }
                    });
    }
  }

  createAccount(accData: NgForm): void {
   this.accountService.createAccount(this.newAccount).then( (newres:any) => {
      if(newres.ok === undefined || newres.ok === 'true') {
      this.showSuccess = true;
      this.blockByName = false;
      this.showError = false;
      this.error = undefined;
      accData.reset();
      this.newAccount = new Account();
        }
      }, error => {
        this.showSuccess = false;
        this.showError = true;
        this.error = `${error.status} ${error.statusText}`;
    });
  }
  


  setAccount(acc: Account): void {
    localStorage.setItem('account_id', acc.accountId);
  }

  clearAccountEditing(): void {
    this.blockByName = false;
    this.blockByPassword = false;
    this.blockByPassword = false;
    this.newPassword = '';
    this.previusPassword = '';
    this.findLogin = '';
  }


  login(): void {
    this.accountService.authorizate(this.LoginAcc.userName, this.LoginAcc.password).then( response => {
      if(response.ok === undefined || response.ok === 'true') {
        this.error = undefined;
        this.blockAuthorization = false;
        console.log("Point 1");
        this.accountService.getAccountByName(this.LoginAcc.userName);
        location.replace('/notes');
      } else {
        console.log('Error', response);
        this.blockAuthorization = true;
        this.error = `${response.status} ${response.statusText}`;
        console.log('Err',response);
      }
    })
  }

  AcceptAuthorization(): void {
    this.blockAuthorization = false;
  }

}
