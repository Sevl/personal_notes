export class Account {
  accountId: string;
  userName: string;
  password: string;
  role: string;
}
