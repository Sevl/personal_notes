import { Injectable, ViewChild, ViewChildren } from '@angular/core';
import { Account } from './Account';
import { Headers, Http } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Message } from '../Admin/Message';

@Injectable()
export class AccountService {
  private baseUrl = 'http://localhost:64086/account';

  constructor(private http: Http,
              private httpClient: HttpClient) {}

  getAccounts(): Promise<Account[]> {
    return this.http
      .get(this.baseUrl)
      .toPromise()
      .then(response => response.json() as Account[])
      .catch(this.handleError);
  }

  getAccountById(id: string): Promise<Account> {
    return this.http
      .get(this.baseUrl + '/' + id)
      .toPromise()
      .then(response => response.json() as Account)
      .catch(this.handleError);
  }

  getAccountByName(name: string): Promise<Account> {
    console.log("Try to find user");
    return this.http
      .get(this.baseUrl + '/get/' + name)
      .toPromise()
      .then(response => response.json() as Account)
      .then(res => {
        localStorage.setItem("acc_Id", res.accountId);
      })
      .catch(this.handleError);
  }

  createAccount(accountData: Account): Promise<Account>  {
    return this.httpClient
      .post(this.baseUrl, accountData)
      .toPromise()
      .then( response =>  {return response},
                  error => {return error});

    /*return this.http
      .post(this.baseUrl, accountData)
      .toPromise()
      .then(response => true)
      .catch(this.handleError);
      */
  }

  updateAccount(accData: Account): Promise<Account> {
    return this.http
      .put(this.baseUrl + '/' + accData.accountId, accData)
      .toPromise()
      .then(response => response.json() as Account)
      .catch(this.handleError);
  }

  deleteAccount(id: string): Promise<any> {
    return this.http
      .delete(this.baseUrl + '/' + id)
      .toPromise()
      .catch(this.handleError);
  }

  authorizate(login: string , password:string) : Promise<any> {
    console.log('Authorize start');
  
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post( this.baseUrl + '/login', 
                           JSON.stringify({ login, password }),{headers}
    )
      .toPromise()
      .then( res => res.json() )
      .then( res => {
                    localStorage.setItem('auth_token', res.auth_token);
                    return res;
                  },
             err => err);
  }

  checkLogin(login: string) : Promise<boolean> {
    var result: boolean;

    return this.http.get( this.baseUrl + '/check/login/' + login)
      .toPromise()
      .then( res => res.json() as boolean)
      .then( res => result = res);
  }

  private handleError(error: any): Promise<any> {
    console.error('Some error occured', error);
    return Promise.reject(error.message || error);
  }
}
