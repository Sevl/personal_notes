export class Note {
    noteId: string;
    name: string;
    description: string;
    status: string;
    accountId: string;
    left:string;
    top:string;
    position: string;
}