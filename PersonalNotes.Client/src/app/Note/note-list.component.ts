import {Component, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import { Note } from './Note';
import { Account } from '../Account/Account';
import { NgForm } from '@angular/forms';
import { NoteService } from './NoteService';
import { AccountService } from '../Account/AccountService'; 
import {ActivatedRoute, Router} from '@angular/router';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html'
})
export class NoteListComponent implements OnInit {
  notes: Note[];
  newNote: Note = new Note();
  idNote: Note = new Note();
  editingNote: Note = new Note();
  compareNote: Note = new Note();
  currentUser: Account = new Account();
  editing = false;
  
  /*
    ERROR
  */
  showError: boolean;
  error: string;

  

  public selectdata: any[] = [
    { name: 'darkseagreen' },
    { name: 'blanchedalmond' }, 
    { name: 'lightblue'},
    { name: 'thistle'},
    { name: 'aquamarine'}
  ];

  constructor(private noteService: NoteService,
              private accountService: AccountService) {}

  ngOnInit(): void {
    this.getNotes();
    this.RememberUser();
  }

  getNotes() : void {
    this.noteService
      .GetNotes()
      .then(response => {
              this.showError = false;
              this.notes = response;
            },
            error => {
              this.showError = true;
              this.error = error.status + '\n' + error.statusText;
            });
  }

  RememberUser() : void {
      this.accountService.getAccountById( localStorage.getItem("acc_Id") )
                        .then( res => {
                                  this.showError = false;
                                  this.currentUser = res;
                               },
                               error => {
                                  this.showError = true;
                                  this.error = error.status + '\n' + error.statusText;
                                });
  }

  createNote(noteData: NgForm): void {
      this.newNote.accountId = localStorage.getItem("acc_Id");
      this.noteService.AddNote(this.newNote).then( res => {
        noteData.reset();
        console.log("Creating Note success");
        this.newNote = new Note();
        this.getNotes();
        this.showError = false;
      }, error => {
        this.showError = true;
        this.error = error.status + '\n' + error.statusText;
      });
  }

  edit() : void {
      this.noteService.EditNote(this.editingNote).then( res => {
          this.showError = false;
      }, error => {
        this.showError = true;
        this.error = error.status + '\n' + error.statusText;
      })
  }

  selectNote(note: Note) : void {
    Object.assign(this.editingNote , note);
    console.log(this.editingNote);
  }

  getNoteById(id: string) : void {
      this.noteService.GetNote(id)
                      .then( res => {
                        this.idNote = res;
                        this.showError = false;
                      }, error => {
                        this.showError = true;
                        this.error = error.status + '\n' + error.statusText;
                      })
  }

  deleteNote() : void {
      this.noteService.DeleteNote(this.editingNote.noteId)
                      .then( res => {
                        this.editingNote = new Note();
                        console.log("Delete note");
                        this.getNotes();
                        this.showError = false;
                      }, error => { 
                        this.showError = true;
                        this.error = error.status + '\n' + error.statusText;
                      })
  }
  
  selectNoteId(id: string) : void {
    localStorage.setItem("note_id",id);
  }
}
