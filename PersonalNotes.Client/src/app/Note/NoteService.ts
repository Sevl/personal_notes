import { Injectable } from '@angular/core';
import { Note } from './Note';
import { Headers, Http } from '@angular/http';


@Injectable()
export class NoteService {
  private baseUrl = 'http://localhost:64086/note';
  headers: Headers = new Headers();
  authToken = localStorage.getItem('auth_token');

  constructor(private http: Http) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', `Bearer ${this.authToken}`);
  }

  GetNotes() : Promise<Note[]> {
      var headers = this.headers;
      return this.http.get(this.baseUrl + "/" + localStorage.getItem("acc_Id"), {headers})
                      .toPromise()
                      .then(response => response.json() as Note[]);
  }

  AddNote(note:Note) : Promise<Note> {
      var headers = this.headers;

      return this.http.post(this.baseUrl, note, {headers})
                      .toPromise()
                      .then(response => response.json() as Note);
  }

  EditNote(note:Note) : Promise<Note> {
      var headers = this.headers;

      return this.http.post(this.baseUrl + "/edit", note, {headers})
                      .toPromise()
                      .then(response => response.json() as Note);
  }

  GetNote(id:string) : Promise<Note> {
      var headers = this.headers;

      return this.http.get(this.baseUrl + "/get/" + id, {headers})
                      .toPromise()
                      .then(response => response.json() as Note);
  }

  DeleteNote(id: string) : Promise<any> {
      var headers = this.headers;

      return this.http.delete(this.baseUrl + "/" + id, {headers})
                      .toPromise();
  }
}