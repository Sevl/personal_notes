import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { AppComponent } from './app.component';
import { AccountListComponent } from './Account/account-list.component';
import { ModalComponent } from './Modal/modal.component';
import { NoteListComponent } from './Note/note-list.component';
import { FinanceListComponent } from './Finance/finance-list.component';
import { AdminListComponent } from './Admin/admin-list.component';
import { PasswordComponent } from './ChangePassword/password.component';

import { AccountService } from './Account/AccountService';
import { NoteService } from './Note/NoteService';
import { FinanceService } from './Finance/FinanceService';
import { MessageService } from './Admin/MessageService';
import { PasswordService } from './ChangePassword/PasswordService';


import { TotalGuard } from './Guards/TotalGuard ';


const appRoutes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: AccountListComponent },
  { path: 'notes',  component: NoteListComponent, canActivate:[TotalGuard]},
  { path: 'finance', component: FinanceListComponent, canActivate:[TotalGuard]},
  { path: 'admin', component: AdminListComponent, canActivate:[TotalGuard]},
  { path: 'password/:token', component: PasswordComponent}
];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule
  ],
  declarations: [
    AppComponent,
    AccountListComponent,
    ModalComponent,
    NoteListComponent,
    FinanceListComponent,
    AdminListComponent,
    PasswordComponent
  ],
  providers: [AccountService, NoteService, FinanceService, MessageService, PasswordService, TotalGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}
