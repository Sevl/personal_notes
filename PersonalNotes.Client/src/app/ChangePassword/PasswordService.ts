import { Injectable, ViewChild, ViewChildren } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class PasswordService {
  private baseUrl = 'http://localhost:64086/password';
  headers: Headers = new Headers();

  constructor(private http: Http,
              private httpClient: HttpClient) {
                this.headers.append('Content-Type', 'application/json');
              }


  checkPassword(id: string, pass: string) : Promise<boolean> {
    var headers = this.headers;
    return this.http.post( this.baseUrl + '/check/' + id, JSON.stringify(pass), {headers})
      .toPromise()
      .then( res => res.json() as boolean);
  }

  changePassword(id : string, pass:string) : any {
    var headers = this.headers;
    return this.http.post( this.baseUrl + '/change/' + id, JSON.stringify(pass), {headers})
        .toPromise()
        .then(res => { 
            location.replace("/login");
        })
  }
}
