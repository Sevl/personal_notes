import {Component, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import { PasswordService } from './PasswordService';
import { Account } from '../Account/Account';
import { MessageService } from '../Admin/MessageService';
import { NgForm } from '@angular/forms';
import { AccountService } from '../Account/AccountService';
import { Message } from '../Admin/Message';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'pass-change',
  templateUrl: './pass.component.html'
})
export class PasswordComponent implements OnInit {
  account: Account = new Account();
  newpass: string;
  confirmPass: string;
  token: any;
  id: string;
  /*
   *   ERRORS 
   */
  existingPass: boolean = false;

  constructor(private accountService: AccountService,
              private passwordService: PasswordService,
              private route: ActivatedRoute
              ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.token = params['token'];

      this.token = JSON.parse(this.token);
      this.id = this.token.id;
      localStorage.setItem("auth_token", this.token.auth_token);
   });
      /*this.accountService.getAccountById(localStorage.getItem("acc_id"))
            .then(res => this.account = res);*/
  }

  comparePass() {
      this.passwordService.checkPassword(this.id, this.newpass)
                            .then(res => { 
                              if ( res == true) this.existingPass = true;
                              else this.existingPass = false;
                            })
  }


  changePass() {
      this.passwordService.changePassword(this.id, this.newpass)
                          .then( /*location.replace("/login")*/);
  }
  

}
