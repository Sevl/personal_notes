import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import { Observable } from "rxjs";
 
export class TotalGuard implements CanActivate{
 
    constructor() {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean {
        if ( localStorage.getItem("auth_token") ) {
            return true;
        }
        location.replace('/login');
        return false;
    }
}