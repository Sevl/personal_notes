/*
*   DRAG AND DROP FILE 
*/

var divPos = {};

function dragStart(elem, e) {
    /*
     *  Координаты мыши в элементе 
     */
    shiftX = e.pageX - elem.offsetParent.offsetLeft - elem.offsetLeft;
    shiftY = e.pageY - elem.offsetParent.offsetTop - elem.offsetTop;

    

    divPos.left = shiftX;
    divPos.top = shiftY;
}

function dragEnd(elem, e) {

    elem.style.position = 'absolute';

    elem.style.left = e.pageX - elem.offsetParent.offsetLeft - divPos.left + 'px';
    elem.style.top = e.pageY - elem.offsetParent.offsetTop - divPos.top + 'px';

    var note;
    
    $.ajax({
        type: "GET",
        headers: {
            "Content-Type" : "application/json",
            "Authorization" : `Bearer ${localStorage.getItem("auth_token")}`
        },
        url: "http://localhost:64086/note/get/" + localStorage.getItem("note_id"),
        success: function(data) {
            note = data;
            note.left = parseInt(elem.style.left);
            note.top = parseInt(elem.style.top);
            note.position = elem.style.position;

            var myJson = JSON.stringify(note); 

            $.ajax({
                type: "POST",
                headers: {
                    "Content-Type" : "application/json",
                    "Authorization" : `Bearer ${localStorage.getItem("auth_token")}`
                },
                url: "http://localhost:64086/note/edit",
                data: myJson,
                success: function() {
                    console.log("Sucess AddCoordinates");
                },
                error: function(data) {
                    alert('Error AddCoordinates', data);
                }
            });
        },
        error: function(data) {
            alert('Error AddCoordinates', data);
        }
    });    
}
