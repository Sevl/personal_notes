import { Injectable, ViewChild, ViewChildren } from '@angular/core';
import { FinanceOperation } from './FinanceOperation';
import { DateFilterModel } from './Filter/DateFilterModel';
import { Http, RequestOptions, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class FinanceService {
  private baseUrl = 'http://localhost:64086/finance';
  headers: Headers = new Headers();
  authToken = localStorage.getItem('auth_token');
  
  
  constructor(private http: Http) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', `Bearer ${this.authToken}`);
  }

  GetOperations() : Promise<FinanceOperation[]> {
      var headers = this.headers;

      return this.http.get(this.baseUrl + "/" + localStorage.getItem("acc_Id"), {headers})
                      .toPromise()
                      .then(response => response.json() as FinanceOperation[]);
  }

  AddOperation(finance:FinanceOperation) : Promise<FinanceOperation> {
      var headers = this.headers;

      return this.http.post(this.baseUrl, finance, {headers})
                      .toPromise()
                      .then(response => response.json() as FinanceOperation);
  }

  EditOperation(finance:FinanceOperation) : Promise<FinanceOperation> {
      var headers = this.headers;

      return this.http.post(this.baseUrl + "/edit", finance, {headers})
                      .toPromise()
                      .then(response => response.json() as FinanceOperation);
  }

  GetOperationById(id:string) : Promise<FinanceOperation> {
      var headers = this.headers;

      return this.http.get(this.baseUrl + "/edit" + id, {headers})
                      .toPromise()
                      .then(response => response.json() as FinanceOperation);
  }

  DeleteOperation(id: string) : Promise<any> {
      var headers = this.headers;

      return this.http.delete(this.baseUrl + "/" + id, {headers})
                      .toPromise();
  }

  filterAll(obj: DateFilterModel) : Promise<FinanceOperation[]> {
    var headers = this.headers;

    return this.http.post(this.baseUrl + "/filter", obj , {headers})
                      .toPromise()
                      .then(response => response.json() as FinanceOperation[]);
  }
}