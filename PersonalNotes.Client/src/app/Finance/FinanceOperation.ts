export class FinanceOperation {
    financeOperationId: string;
    name: string;
    type: string;
    value: string;
    dateOfOperation: Date;
    accountId: string;
}