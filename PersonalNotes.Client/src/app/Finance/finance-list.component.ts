import {Component, OnInit} from '@angular/core';
import { FinanceOperation } from './FinanceOperation';
import { DateFilterModel } from './Filter/DateFilterModel';
import { NgForm, FormControl } from '@angular/forms';
import { FinanceService } from './FinanceService';
import { AccountService } from '../Account/AccountService';
import {Account} from '../Account/Account';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-oper-list',
  templateUrl: './oper-list.component.html'
})
export class FinanceListComponent implements OnInit {
  operations: FinanceOperation[];
  compareOperations: FinanceOperation[];
  newOperation: FinanceOperation = new FinanceOperation();
  idOperation: FinanceOperation = new FinanceOperation();
  editingOperation: FinanceOperation = new FinanceOperation();
  compareOperation: FinanceOperation = new FinanceOperation();
  currentUser: Account = new Account();
  createDate: string;
  totalVal: number = 0;
  editing = false;

  /*
  *  ERROR
  */
  showError: boolean;
  error: string;

  /*
    FILTERS
  */
  filterModel: DateFilterModel = new DateFilterModel();
  filtrationDate: Date;
  filterName: string = "";
  showOnly: string;

  public selectdata: any[] = [
    { name: 'Income' },
    { name: 'Expenditure' }
  ];

  public dates: any[] = [];

  constructor(private financeService: FinanceService,
              private accountService: AccountService) {}

  ngOnInit(): void {
    this.getOperations();
    this.RememberUser();
  }

  getOperations() : void {
    this.financeService
      .GetOperations()
      .then(response => {
        this.operations = response;
        this.compareOperations = this.operations;
        this.showError = false;
        this.calculateTotal(response);
      }, error => {
        this.showError = true;
        this.error = error.status + "\n" + error.statusText;
      });
  }

  calculateTotal(arr: FinanceOperation[]) {
    for (var i = 0; i < arr.length ; i++) {
      if (arr[i].type == 'Income') {
        this.totalVal += +arr[i].value;
      } else {
        this.totalVal -= +arr[i].value;
      }
    }
  }

  setShow(val: string) {
    this.filterModel.ShowOnly = val;
  }

  setValFilter(val: string) {
    this.filterModel.ValueFilter = val;
  }

  RememberUser() : void {
      this.accountService.getAccountById( localStorage.getItem("acc_Id") )
                        .then( res => this.currentUser = res );
  }

  createOperation(operData: NgForm): void {
      this.newOperation.accountId = localStorage.getItem("acc_Id");
      console.log(this.createDate);

      this.financeService.AddOperation(this.newOperation).then( res => {
        operData.reset();
        this.newOperation = new FinanceOperation();
        this.getOperations();
      }, res => { console.log("Creating Operation failed")});
  }

  editOperation(editData: NgForm) : void {
      this.financeService.EditOperation(this.editingOperation).then( res => {
          editData.reset();
          this.editingOperation = new FinanceOperation();
          this.getOperations();
      }, res => {
          console.log("Editing failed");
          console.log(res);
      })
  }

  selectOperation(financeOperation: FinanceOperation) : void {
    Object.assign(this.editingOperation , financeOperation);
  }

  getOperationById(id: string) : void {
      this.financeService.GetOperationById(id)
                      .then( res => {
                        this.idOperation = res;
                        console.log("Operation load by id");
                      }, res => {
                        console.log("Error", res);
                      })
  }

  deleteOperation() : void {
      this.financeService.DeleteOperation(this.editingOperation.financeOperationId)
                      .then( res => {
                        this.editingOperation = new FinanceOperation();
                        console.log("Delete operation");
                        this.getOperations();
                      }, res => { console.log("Failed(Delete note)")})
  }

  filterByName() : void {
    var filter = this.filterName;
    this.operations = this.compareOperations.filter( function(element) {
      return element.name.includes(filter.trim());
    });
  }

  applyFilter() : void {
    this.financeService.filterAll(this.filterModel)
        .then(res => {
          this.operations = res;
          this.compareOperations = res;
        });
  }

  check() : void {
    console.log("DATE CHANGE");
    console.log(this.createDate);
    console.log(this.newOperation);
    console.log(this.newOperation.dateOfOperation);
  }

}
